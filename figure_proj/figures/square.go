package figures

import "figure_proj/primitives"

func Square_area(x Square) float64 {
	a := primitives.Distance(x.X1, x.Y1, x.X2, x.Y2)
	return a * a
}
