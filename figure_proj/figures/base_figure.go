package figures

type Square struct {
	X1, Y1, X2, Y2 float64
}

type Circle struct {
	X1, Y1, X2, Y2 float64
}

type Triangle struct {
	X1, Y1, X2, Y2, X3, Y3 float64
}
