package figures

import (
	"math"
	"figure_proj/primitives"
)

func Triangle_area(x Triangle) float64 {
	a := primitives.Distance(x.X1, x.Y1, x.X2, x.Y3)
	b := primitives.Distance(x.X1, x.Y1, x.X3, x.Y2)
	c := primitives.Distance(x.X2, x.Y2, x.X3, x.Y3)
	p := (a + b + c) / 2
	return math.Sqrt(p * (p - a) * (p - b) * (p - c))
}
