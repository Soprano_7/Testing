package figures

import (
	"math"
	"figure_proj/primitives"
)

func Circle_area(x Circle) float64 {
	r := primitives.Distance(x.X1, x.Y1, x.X2, x.Y2)
	return math.Pi * r * r
}
