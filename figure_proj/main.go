package main

import (
	"figure_proj/figures"
	"fmt"
)

func main() {
	square := figures.Square{2, 3, 4, 6}
	circle := figures.Circle{2, 3, 4, 6}
	triangle := figures.Triangle{2, 2, 3, 5, 6, 8}
	fmt.Println(figures.Square_area(square))
	fmt.Println(figures.Circle_area(circle))
	fmt.Println(figures.Triangle_area(triangle))
}
